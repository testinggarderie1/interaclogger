const messagesLoader = require('./loadMessages');
const interacMessageFilter = require('./filterInteracMessages');
const interacMessagesPublisher = require('./topicMessagePublisher');

const gmailMessagesPath = '/tmp/messages.txt';

exports.loadNewInteracMessageInPubSub = async (req, res) => {

    const operation = req.body.operation;

    if(operation !== 'loadMessages' && operation !== 'loadRecentMessages'){
      const errorMessage = `operation ${operation} is invalid`;
      res.status(400).send(errorMessage).end();
    }

    const filetringDate = req.body.filtringDate;
    const filteringLabels = req.body.filteringLabels;


    await messagesLoader.loadGmailMessages(gmailMessagesPath, operation);


    const paymentInfos = interacMessageFilter.getPaymentInfosFromInteracMessages(
      gmailMessagesPath, filetringDate, filteringLabels
    );

    if(paymentInfos.length !== 0){
        console.log(`Published ${paymentInfos.length} new Interac Messages`);
        const result = JSON.stringify({ messages : paymentInfos });

        await interacMessagesPublisher.publishInteracMessage(result);

        res.status(200).send().end();
    }else{
        console.log('no new Interac Messages...')
    }



    // if(operation === 'changeMessagesLabels'){
    //     const messages = req.body.messages;
    //
    //     for(const message of messages){
    //         const messageId = message.messageId;
    //         const messageLabels = message.messageLabels;
    //     }
    // }
    

};
