const request = require('request');
const {google} = require('googleapis');
const base64url = require('base64url');
const fs = require('fs');

const getClientSecretUrl = "https://us-central1-manager-218900.cloudfunctions.net/getClientSecret";

async function deployedFunction(url, form){
  return new Promise(function (resolve, reject) {
    request.post(url, {
      form: form
    }, (err, res) => {
      if (err) { reject(err); } else { resolve(res); }
    });
  });
}

async function createOAuthClient(){
  const GCF_REGION = 'us-central1';
  const GCLOUD_PROJECT = 'manager-218900';
  const clientSecretJson = await deployedFunction(getClientSecretUrl);
  const clientInfos = JSON.parse(clientSecretJson.body);

  let authClient = new google.auth.OAuth2(
    clientInfos.clientId,
    clientInfos.clientSecret,
    `https://${GCF_REGION}-${GCLOUD_PROJECT}.cloudfunctions.net/oauth2callbackTest`
  );

  authClient.setCredentials({
    refresh_token: clientInfos.refreshToken
  });

  return authClient;
}

function concatenateMessagePartsToExtratBody(entity, data){
  if(entity.parts){
    for(let part of entity.parts){
      data += concatenateMessagePartsToExtratBody(part, data);
    }
  }
  if(!entity.parts){
    if(entity.body.data){
      data += base64url.decode(entity.body.data);
    }
  }
  return data;
}

function extractElementFromMessage(element, messageHeaders){
  for(let headerPart of messageHeaders){
    if(headerPart.name === element){
      return headerPart.value;
    }
  }
}

function addMessageToJsonFile(messageObject, messagesFilePath){
  try {
    fs.appendFileSync(messagesFilePath, JSON.stringify(messageObject)+'\n');
    console.log('Saved');
  } catch (err) {
    throw err
  }
}


function parseGmailMessage(messageContent, message_id){
  let message = {
    id : message_id,
    from : "",
    to : "",
    date : "",
    subject : "",
    content : ""
  };

  message.subject = extractElementFromMessage("Subject", messageContent.data.payload.headers);
  message.from = extractElementFromMessage("From", messageContent.data.payload.headers);
  message.to = extractElementFromMessage("To", messageContent.data.payload.headers);
  message.date = extractElementFromMessage("Date", messageContent.data.payload.headers);
  message.content = concatenateMessagePartsToExtratBody(messageContent.data.payload, message.content);
  message.labels = messageContent.data.labelIds;
  return message;
}

exports.loadGmailMessages = async (messagesFilePath, operation) => {
  if(!operation){
    throw new Error("no operation specified for loading the messages");
  }
  const oauth2client = await createOAuthClient();

  const gmail = google.gmail({
    version: 'v1',
    auth: oauth2client,
  });

  let options = { userId: 'me', };

  if(operation === 'loadRecentMessages'){
    const secondSinceEpochForTheLast24Hours = parseInt((Date.now()/1000)-(24*3600));
    options = {
      userId: 'me',
      q: `after:${secondSinceEpochForTheLast24Hours}`
    };
  }

  const messages = await gmail.users.messages.list(options);
  const messageIds = messages.data.messages.map(el => el.id);

  fs.open(messagesFilePath, 'w', function (err) {
    if (err){console.log(err)}
    console.log('File is opened in write mode.');
  });

  for (let message_id of messageIds) {

    const messageContent = await gmail.users.messages.get({ userId: 'me', id: message_id, format: "full" });

    await addMessageToJsonFile(parseGmailMessage(messageContent, message_id), messagesFilePath);
  }
};


