const mockRes = require('jest-mock-express').response;
const publishInteracMessages = require('../getInteracMessages/index');

describe('## filterInteracMessages', () => {
  describe('# getPaymentInfosFromInteracMessages', () => {
    it('should return all and only interac messages', async () => {
      const testRequest = {body:{operation:"loadRecentMessages"}};

      const res = mockRes();
      await publishInteracMessages.loadNewInteracMessageInPubSub(testRequest, res);
      expect(true).toBe(true);
    });
  });
});