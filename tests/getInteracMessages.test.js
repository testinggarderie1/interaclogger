const mockRes = require('jest-mock-express').response;
const httpStatus = require('http-status');
const request = require('request');

const getInteracMessages = require('../getInteracMessages/loadMessages');

const getInteracMessagesURL = "https://us-central1-manager-218900.cloudfunctions.net/getGmailInteracMessages";
const deployed = false;

async function testDeployed(url, form){
  return new Promise(function (resolve, reject) {
    request.post(url, {
      form: form
    }, (err, res) => {
      if (err) { reject(err); } else { resolve(res); }
    });
  });
}

describe('## getInteracMessage', () => {
  it('should return Interac messages', async () => {
    const gmailMessagesPath = './modules/InteracLogger/getInteracMessages/GmailMessages/messages.txt';
    if (!deployed) {
      const result = await getInteracMessages.loadGmailMessages(gmailMessagesPath);
      expect(typeof result).toBe('object');
    }

    if (deployed) {
      jest.setTimeout(10000);
      const result = await testDeployed(getInteracMessagesURL, testRequest);
      expect(typeof result).toBe('object');
    }
  });

  it('should return load interacMessages from historyId', async () => {
    const gmailMessagesPath = './modules/InteracLogger/getInteracMessages/GmailMessages/messages.txt';
    const historyId = '7702';
    if (!deployed) {
      const result = await getInteracMessages.loadMessageOfGmailHistoryId(gmailMessagesPath, historyId);
      expect(typeof result).toBe('object');
    }

    if (deployed) {
      jest.setTimeout(10000);
      const result = await testDeployed(getInteracMessagesURL, testRequest);
      expect(typeof result).toBe('object');
    }
  });
});