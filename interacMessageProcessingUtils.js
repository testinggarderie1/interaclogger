exports.isInteractMessage = (messageObject) => {
  //TODO: Improve security on checking interac messages
  if(!messageObject){
    return false;
  }
  let searchInteracTermsOnSubject = /INTERAC|accepted your request|sent you money|has been automatically deposited|vous a envoyé des fonds/;

  let found = messageObject.subject.match(searchInteracTermsOnSubject);
  if(found !== null){
    return true;
  }
  return false;
};

exports.fromStringToMessagesObject = (messagesString) => {
  if(messagesString){
    return JSON.parse(messagesString);
  }
};

exports.getMessageID = (messageObject) => {
  return messageObject.id;
};

exports.getMessageContentEncoded = (messageObject)=> {
  if(messageObject.content){
    return Buffer.from(messageObject.content).toString('base64');
  }
};


exports.getPersonNameWhoSentMoneyFromMessage = (message) => {
  if(message.subject){
    const subject = message.subject;
    let found = subject.match(/e-Transfer: .* accepted|e-Transfer: .* sent|transfer from .* has been|Virement INTERAC : .* vous a envoyé des fonds/);
    if(found !== null){
      found = found[0].replace('e-Transfer: ', '').replace(' accepted', '').replace(' sent', '')
      .replace('transfer from ', '').replace(' has been', '').replace('Virement INTERAC : ', '')
      .replace(' vous a envoyé des fonds', '');
    }
    return found;
  }
};

exports.getMoneySentFromMessage = (message) => {
  if(message.content){
    const body = message.content;
    let found = body.match(/\$\d+.\d+\s\(CAD\)|\d+.\d+\s\$ \(CAD\)/);
    if(found !== null){
      found = found[0].replace('$', '')
      .replace('(CAD)', '').replace(',', '.')
      .replace(/ /g, '');
    }
    return found;
  }
};

exports.getFamilyIDFromMessage = (message) => {
  if(message.content){
    const body = message.content;
    let found = body.match("genie\\d+");
    if(found !== null){
      found = found[0].replace('e-Transfer: ', '').replace(' accepted', '').replace(' sent', '')
      .replace('transfer from ', '').replace(' has been', '')

    }
    return found;
  }
};

exports.getDateOfPayment = (message) => {
  return message.date;
};