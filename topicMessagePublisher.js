const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub();

const interacMessagesTopic = 'NewInteracMessages';

exports.publishInteracMessage = async (data) => {
  const dataBuffer = Buffer.from(data);

  const messageId = await pubsub
  .topic(interacMessagesTopic)
  .publisher()
  .publish(dataBuffer);

  console.log(messageId);

};
