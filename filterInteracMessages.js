const fs = require('fs');
const interacMessagesUtil = require('./interacMessageProcessingUtils');


exports.getPaymentInfosFromInteracMessages = (gmailMessagesPath, date, labels) => {

  let interactMessages = [];

  let fileContent = fs.readFileSync(gmailMessagesPath, "utf8");

  const lines = fileContent.split('\n');
  for(const line of lines){
    const messageObject = interacMessagesUtil.fromStringToMessagesObject(line);
    if(interacMessagesUtil.isInteractMessage(messageObject)
      && applyFilters(messageObject, date, labels)){
        interactMessages.push(getPaymentsInfos(messageObject));
      }
    }

  return interactMessages;
  };

function applyFilters(messageObject, date, labels){
  return filterByDate(messageObject, date) && filterByLabel(messageObject, labels);
}

function filterByDate(messageObject, limitDate){
  if(limitDate){
    const messageDate = Date.parse(messageObject.date);
    const desiredLimitDate = Date.parse(limitDate);

    return messageDate >= desiredLimitDate;

  }
  return true;
}

function filterByLabel(messageLabels, desiredLabels){
  if(desiredLabels){
    return messageLabels.labels.some(r => desiredLabels.indexOf(r) >= 0);
  }
  return true;
}

function getPaymentsInfos(messageObject){
  const result = {
    familyId : interacMessagesUtil.getFamilyIDFromMessage(messageObject),
    moneySent : interacMessagesUtil.getMoneySentFromMessage(messageObject),
    sender : interacMessagesUtil.getPersonNameWhoSentMoneyFromMessage(messageObject),
    dateOfPayment : interacMessagesUtil.getDateOfPayment(messageObject),
    messageID : interacMessagesUtil.getMessageID(messageObject),
    encodedMessageContent : interacMessagesUtil.getMessageContentEncoded(messageObject)
  };

  return result;
}
